import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JApplet;

public class HelloWorld extends JApplet implements Runnable {
	private boolean keepGoing;
	private Thread thread;
    private int startx;
	
	public void init() {
		keepGoing = true;
		
		thread = new Thread(this);
		
		thread.start();
	}
	
	public void destroy() {
		keepGoing = false;
	}
	
	private void pause(double seconds) {
		try {
			Thread.sleep((int)(seconds*1000));
		} catch (InterruptedException ie) {
			System.out.println(ie);
		}
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		
		g.setFont(new Font("Comic Sans MS",Font.BOLD,20));
		
		FontMetrics metrics = g.getFontMetrics();
		
		int width = metrics.stringWidth("Hello World");
		
		if (startx + width < 0)
			startx = getWidth();
		
		int height = metrics.getHeight();
		
		g.drawString("Hello World", startx, getHeight()/2 + height/4);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		while (keepGoing) {
			startx -= 10;
			
			pause(0.1);
			
			repaint();
		}
		
	}

}
